import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  setPostReaction,
  setCommentReaction,
  toggleExpandedPost,
  getPostReactions,
  getCommentReactions,
  addComment,
  deletePost,
  deleteComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  setPostReaction: postReaction,
  setCommentReaction: commentReaction,
  getPostReactions: loadPostReactions,
  getCommentReactions: loadCommentReactions,
  toggleExpandedPost: toggle,
  addComment: add,
  deletePost: delPost,
  deleteComment: delComment
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            setPostReaction={postReaction}
            getPostReactions={loadPostReactions}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            deletePost={delPost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  setCommentReaction={commentReaction}
                  getCommentReactions={loadCommentReactions}
                  deleteComment={delComment}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  setPostReaction: PropTypes.func.isRequired,
  setCommentReaction: PropTypes.func.isRequired,
  getPostReactions: PropTypes.func.isRequired,
  getCommentReactions: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  setPostReaction,
  setCommentReaction,
  getPostReactions,
  getCommentReactions,
  toggleExpandedPost,
  addComment,
  deletePost,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
