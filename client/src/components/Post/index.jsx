import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import LabelLike from '../LabelLike';

import styles from './styles.module.scss';

const Post = ({ post, setPostReaction, getPostReactions, toggleExpandedPost, sharePost, deletePost, currentUser }) => {
  const {
    id,
    image,
    body,
    user,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <LabelLike entity={post} getReactions={getPostReactions} setReaction={setPostReaction} />
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setPostReaction(id, false)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {
          currentUser.id === user.id
          && (
            <Label
              basic
              size="small"
              as="a"
              className={`${styles.toolbarBtn} right floated`}
              onClick={() => deletePost(id)}
            >
              <Icon name="trash" />
            </Label>
          )
        }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  setPostReaction: PropTypes.func.isRequired,
  getPostReactions: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  currentUser: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = rootState => ({
  currentUser: rootState.profile.user
});

export default connect(mapStateToProps)(Post);
