import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, Popup } from 'semantic-ui-react';

import styles from './styles.module.scss';

const LabelLike = ({ entity, setReaction, getReactions }) => {
  const withPopup = entity.likeCount > 0;

  useEffect(() => {
    if (withPopup) {
      getReactions(entity.id);
    }
  }, []);

  const BaseLabel = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={() => setReaction(entity.id, true)}
    >
      <Icon name="thumbs up" />
      {entity.likeCount}
    </Label>
  );

  return withPopup ? (
    <Popup
      position="bottom left"
      inverted
      size="small"
      mouseEnterDelay={500}
      style={{ zIndex: 999999999999 }}
      trigger={BaseLabel}
    >
      <Popup.Content>
        {
          entity.reactions
          && entity.reactions.map(reaction => reaction.isLike
            && (
              <Fragment key={reaction.id}>
                {reaction.userName}
                <br />
              </Fragment>
            ))
        }
      </Popup.Content>
    </Popup>
  ) : BaseLabel;
};

LabelLike.propTypes = {
  entity: PropTypes.objectOf(PropTypes.any).isRequired,
  setReaction: PropTypes.func.isRequired,
  getReactions: PropTypes.func.isRequired
};

export default LabelLike;
