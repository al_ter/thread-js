import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import LabelLike from '../LabelLike';

import styles from './styles.module.scss';

const Comment = ({ comment, setCommentReaction, getCommentReactions, deleteComment, currentUser }) => {
  const { id, body, createdAt, user, dislikeCount } = comment;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
          <LabelLike entity={comment} getReactions={getCommentReactions} setReaction={setCommentReaction} />
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setCommentReaction(id, false)}>
            <Icon name="thumbs down" />
            {dislikeCount}
          </Label>
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        {
          currentUser.id === user.id
          && (
            <CommentUI.Actions>
              <CommentUI.Action onClick={() => deleteComment(id)}>Delete</CommentUI.Action>
            </CommentUI.Actions>
          )
        }
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  setCommentReaction: PropTypes.func.isRequired,
  getCommentReactions: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  currentUser: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = rootState => ({
  currentUser: rootState.profile.user
});

export default connect(mapStateToProps)(Comment);
