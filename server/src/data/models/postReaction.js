export default (orm, DataTypes) => {
  const PostReaction = orm.define('postReaction', {
    isLike: {
      type: DataTypes.BOOLEAN
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return PostReaction;
};
