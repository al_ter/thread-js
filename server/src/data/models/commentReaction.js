export default (orm, DataTypes) => {
  const CommentReaction = orm.define('commentReaction', {
    isLike: {
      type: DataTypes.BOOLEAN
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return CommentReaction;
};
