import { CommentReactionModel, CommentModel, UserModel } from '../models/index';
import sequelize from '../db/connection';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'comment.id'
      ],
      where: { userId, commentId },
      include: [{
        model: CommentModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getCommentReactions(commentId) {
    return this.model.findAll({
      raw: true,
      attributes: [
        'id', 'isLike', 'userId',
        [sequelize.col('user.username'), 'userName']
      ],
      where: { commentId },
      include: {
        model: UserModel,
        attributes: []
      }
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
