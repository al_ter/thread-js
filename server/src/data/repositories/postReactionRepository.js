import { PostReactionModel, PostModel, UserModel } from '../models/index';
import sequelize from '../db/connection';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getPostReactions(postId) {
    return this.model.findAll({
      raw: true,
      attributes: [
        'id', 'isLike', 'userId',
        [sequelize.col('user.username'), 'userName']
      ],
      where: { postId },
      include: {
        model: UserModel,
        attributes: []
      }
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
